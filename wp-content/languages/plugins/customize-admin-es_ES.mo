��            )         �     �  ,   �     �  
   �     �          #     4     D  E   \  G   �     �     �     	     "     <     P     f     z  ?   �  D   �  E     D   R  G   �  <   �  J     a   g     �  B   �        &  ;  	   b  0   l     �     �     �     �      �     	  (   #	  T   L	  S   �	     �	     
     
  '   9
     a
     |
     �
     �
  Q   �
  N     P   d  N   �  `     M   e  X   �  l        y  B   �     �        	                                    
                                                                                            Activity Add your own css to the WordPress dashboard. Choose Image Custom CSS Custom Login Background Custom Logo Custom Logo Link Customize Admin Customize Admin Options Enter a URL or upload logo image. Maximum height: 70px, width: 310px. If not specified, clicking on the logo will return you to the homepage. Johan van der Wijk Quick Draft Remove Dashboard Widgets Remove Generator Meta Tag Remove RSD Meta Tag Remove RSS feed links Remove WLW Meta Tag Save Changes Selecting this option removes the Quick Press dashboard widget. Selecting this option removes the RSD meta tag from the html source. Selecting this option removes the RSS feed link from the html source. Selecting this option removes the WLW meta tag from the html source. Selecting this option removes the WordPress Site News dashboard widget. Selecting this option removes the activity dashboard widget. Selecting this option removes the generator meta tag from the html source. This plugin allows you to customize the appearance and branding of the WordPress admin interface. WordPress Site News https://vanderwijk.com/wordpress/wordpress-customize-admin-plugin/ https://www.vanderwijk.com PO-Revision-Date: 2019-01-30 20:11:34+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Customize Admin - Stable (latest release)
 Actividad Añade tu propio css al escritorio de WordPress. Elegir imagen CSS personalizado Fondo de acceso personalizado Logotipo personalizado Enlace de logotipo personalizado Customize Admin Personalizar opciones de administración Introduce una URL o sube la imagen del logotipo. Altura máxima: 70px, ancho: 310px. Si no se especifica, al hacer clic en el logotipo volverás a la página de inicio. Johan van der Wijk Borrador rápido Eliminar widgets del escritorio Eliminar la etiqueta meta del generador Eliminar etiqueta meta RSD Eliminar enlaces de feeds RSS Quitar etiqueta meta WLW Guardar cambios Seleccionando esta opción, se elimina el widget borrador rápido del escritorio. Seleccionando esta opción, se elimina la etiqueta meta RSD de la fuente html. Seleccionando esta opción, se elimina el enlace del feed RSS de la fuente html. Seleccionando esta opción, se elimina la etiqueta meta WLW de la fuente html. Seleccionando esta opción, se elimina el widget eventos y noticias de WordPress del escritorio. Seleccionando esta opción, se elimina el widget de actividad del escritorio. Seleccionando esta opción, se elimina la etiqueta meta del generador de la fuente html. Este plugin te permite personalizar la apariencia y la marca de la interfaz de administración de WordPress. Eventos y noticias de WordPress https://vanderwijk.com/wordpress/wordpress-customize-admin-plugin/ https://www.vanderwijk.com 